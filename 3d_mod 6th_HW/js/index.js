'use strict'

const GET_IP_URL = 'https://api.ipify.org/?format=json';
const getUrlForAddress = (ip) => {
  return `http://ip-api.com/json/${ip}?fields=status,continent,country,regionName,city,district`
}

const clientAddress = document.getElementsByClassName('client-address')[0];
const searchButton = document.getElementsByClassName('search-button')[0];
searchButton.addEventListener('click', searchClientByIp);

async  function searchClientByIp(){
  const ip = await getIpAddress();
  const address = await getAddress(ip);
  renderAddress(address);
}

async function getAddress(ip){
  const url = getUrlForAddress(ip);
  const response = await doGetRequest(url);
  if (response.status == 200){
    return response.data;
  }
}

async function getIpAddress(){
  const response = await doGetRequest(GET_IP_URL);
  return response.data.ip;
}

async function doGetRequest(url, data = ''){
  return await axios({
    method: 'GET',
    url: url,
  })
}

function renderAddress(data){
 const ul = document.createElement('ul');
 const continent = document.createElement('li');
  continent.textContent = `Continent: ${data.continent}`;
  ul.style.listStyleType = "none";

  ul.appendChild(continent);
  
  const country = document.createElement('li');
  country.textContent = `Country: ${data.country}`
  ul.appendChild(country);

  const district = document.createElement('li');
  district.textContent = `Region: ${data.regionName}`
  ul.appendChild(district);
  clientAddress.append(ul);

  const city = document.createElement('li');
  city.textContent = `City: ${data.city}`;
  ul.appendChild(city);
  
  const region = document.createElement('li');
  region.textContent = `District: ${data.district}`;
  ul.appendChild(region);
}

