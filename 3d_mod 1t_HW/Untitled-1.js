const sum = (...param) => {
  const args = [...param];

  return (args.reduce((a, b) => a + b));

};

console.log(sum(1, 2, 3, 4));

