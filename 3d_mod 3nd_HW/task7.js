const array = ['value', () => 'showValue'];

const {value = 'value', showValue = () => {
    return 'showValue';
}} = array;


alert(value); // должно быть выведено 'value'
alert(showValue());  // должно быть выведено 'showValue'

