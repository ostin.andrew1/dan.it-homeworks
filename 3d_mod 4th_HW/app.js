const filmWrapper = document.querySelector(".film__wrapper");
const filmCharacters = document.querySelector(".film_characters");

const Url = "https://ajax.test-danit.com/api/swapi/films";

function getFilmList() {
    fetch(Url)
        .then((response) => response.json())
        .then((res) => {
            const result = [...res];
            const sortedRes = result.sort((a, b) => a.episodeId - b.episodeId);
            const filmList = sortedRes.map((item) => {
                const wrapperItem = document.createElement("div");
                const filmId = document.createElement("p");
                const filmName = document.createElement("p");
                const filmDescription = document.createElement("p");

                filmId.innerText = `episode ${item.episodeId}`;
                filmName.innerText = `episode ${item.name}`;
                filmDescription.innerText = `episode ${item.openingCrawl}`;

                
                filmWrapper.append(wrapperItem);
                wrapperItem.append(filmId, filmName, filmDescription);
                return wrapperItem;
            });
        }
    );
    getCharacters();
}

function getCharacters(index = 0) {
    fetch(Url)
        .then((response) => response.json())
        .then((res) => {
            const result = [...res];
            const sortedRes = result.sort((a, b) => a.episodeId - b.episodeId);

            const charactersList = sortedRes.map((item) => {
                const charactersArr = [...item.characters];
                return charactersArr; 
            });
            
            const charactersTest = charactersList.map((arr) => {
                let divChildren = filmWrapper.children[index];

                let filmUlList = document.createElement("ul");
                filmUlList.innerText = "characters list";

                let arrList = arr.map((data) => {
                    let liArr = document.createElement('li');

                    fetch(data)
                    .then((data) => data.json())
                    .then((item) => { 
                        liArr.innerText = `${item.name}`;
                        filmUlList.append(liArr);
                        });
                        filmUlList.append(liArr);
                    });

                    divChildren.append(filmUlList);
                    filmCharacters.append(divChildren);

                });
            });
}

getFilmList();
