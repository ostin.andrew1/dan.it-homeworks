const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];


  class IncompleteBook {
    constructor(message) {
        this.message = message;
    }
    name = 'IncompleteBook';
}

function completeBook(book) {
    if (!book.hasOwnProperty('author')){
        throw new IncompleteBook(`У книги ${book['name']}, ${book['price']} не указан автор`);
    }
    if (!book.hasOwnProperty('name')){
        throw new IncompleteBook(`У книги ${book['author']}, ${book['price']} не указано название`);
    }
    if (!book.hasOwnProperty('price')){
        throw new IncompleteBook(`У книги ${book['author']}, ${book['name']} не указана цена`);
    }
}


function renderBooks(books){
    
    const root = document.getElementById('root');
    const booksUl = document.createElement('ul');

    const ulBook = document.createElement('li');
    const ulBookValue = document.createElement('ul');

    for (let book of books) {
        try {
            completeBook(book);
            let newBookUl = createBook(book, ulBook, ulBookValue);
            booksUl.appendChild(newBookUl);
        } catch (e){
            if (e.name == 'IncompleteBook'){
                console.log(e.message);
            } else throw new Error(e.message);
        }
    }
    root.appendChild(booksUl);
}



function createBook(book, ulBook, ulBookValue) {

    let newBookUl = ulBook.cloneNode(true);

    let newBookUlValue = ulBookValue.cloneNode(true);
    newBookUlValue.textContent = book.author;
    newBookUl.appendChild(newBookUlValue);

    newBookUlValue = ulBookValue.cloneNode(true);
    newBookUlValue.textContent  = book.name;
    newBookUl.appendChild(newBookUlValue);

    newBookUlValue = ulBookValue.cloneNode(true);
    newBookUlValue.textContent  = book.price;
    newBookUl.appendChild(newBookUlValue);
    
    return newBookUl;
}

renderBooks(books);
